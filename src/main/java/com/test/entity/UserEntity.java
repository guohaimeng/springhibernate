package com.test.entity;

/**
 * Created by Administrator on 2018/10/16.
 */
public class UserEntity {


    private Integer uid;

    private String account;

    public UserEntity(Integer uid, String account) {
        this.uid = uid;
        this.account = account;
    }

    public String getAccount() {

        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}

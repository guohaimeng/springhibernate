package com.test.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/10/16.
 */
@Entity
@Table(name = "tb_advert", schema = "hqmpms", catalog = "")
public class TbAdvertEntity {
    private long advertId;
    private String title;
    private byte priorityLevel;
    private Timestamp beginTime;
    private Timestamp endTime;
    private Timestamp createTime;
    private long creatorId;
    private String linkUrl;
    private byte type;
    private Byte location;
    private byte isEnable;
    private byte immediateShow;
    private long clicks;
    private byte isDelete;
    private long productId;
    private String advertDesc;

    @Id
    @Column(name = "advert_id")
    public long getAdvertId() {
        return advertId;
    }

    public void setAdvertId(long advertId) {
        this.advertId = advertId;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "priority_level")
    public byte getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(byte priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    @Basic
    @Column(name = "begin_time")
    public Timestamp getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    @Basic
    @Column(name = "end_time")
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "creator_id")
    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    @Basic
    @Column(name = "link_url")
    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    @Basic
    @Column(name = "type")
    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    @Basic
    @Column(name = "location")
    public Byte getLocation() {
        return location;
    }

    public void setLocation(Byte location) {
        this.location = location;
    }

    @Basic
    @Column(name = "is_enable")
    public byte getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(byte isEnable) {
        this.isEnable = isEnable;
    }

    @Basic
    @Column(name = "Immediate_show")
    public byte getImmediateShow() {
        return immediateShow;
    }

    public void setImmediateShow(byte immediateShow) {
        this.immediateShow = immediateShow;
    }

    @Basic
    @Column(name = "clicks")
    public long getClicks() {
        return clicks;
    }

    public void setClicks(long clicks) {
        this.clicks = clicks;
    }

    @Basic
    @Column(name = "is_delete")
    public byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(byte isDelete) {
        this.isDelete = isDelete;
    }

    @Basic
    @Column(name = "product_id")
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "advert_desc")
    public String getAdvertDesc() {
        return advertDesc;
    }

    public void setAdvertDesc(String advertDesc) {
        this.advertDesc = advertDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbAdvertEntity that = (TbAdvertEntity) o;

        if (advertId != that.advertId) return false;
        if (priorityLevel != that.priorityLevel) return false;
        if (creatorId != that.creatorId) return false;
        if (type != that.type) return false;
        if (isEnable != that.isEnable) return false;
        if (immediateShow != that.immediateShow) return false;
        if (clicks != that.clicks) return false;
        if (isDelete != that.isDelete) return false;
        if (productId != that.productId) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (beginTime != null ? !beginTime.equals(that.beginTime) : that.beginTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (linkUrl != null ? !linkUrl.equals(that.linkUrl) : that.linkUrl != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (advertDesc != null ? !advertDesc.equals(that.advertDesc) : that.advertDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (advertId ^ (advertId >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (int) priorityLevel;
        result = 31 * result + (beginTime != null ? beginTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (int) (creatorId ^ (creatorId >>> 32));
        result = 31 * result + (linkUrl != null ? linkUrl.hashCode() : 0);
        result = 31 * result + (int) type;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (int) isEnable;
        result = 31 * result + (int) immediateShow;
        result = 31 * result + (int) (clicks ^ (clicks >>> 32));
        result = 31 * result + (int) isDelete;
        result = 31 * result + (int) (productId ^ (productId >>> 32));
        result = 31 * result + (advertDesc != null ? advertDesc.hashCode() : 0);
        return result;
    }
}

package com.test.controller;

import com.test.dao.AdvertDao;
import com.test.entity.TbAdvertEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Administrator on 2018/10/16.
 */
@Controller
@RequestMapping("/stage")//Contoller下所有接口统一入口
public class TbAdvertController {

    //映射一个action
    @RequestMapping("/stageList")
    @ResponseBody
    public List<TbAdvertEntity> getUser() {
        AdvertDao dao = new AdvertDao();
        //查询stage表的所有数据，返回json
        return dao.query();
    }


}

package com.test.dao;

import com.test.entity.TbAdvertEntity;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by Administrator on 2018/10/16.
 */
public class AdvertDao {

    /**
     * 查询Stage表所有数据
     * */
    public List<TbAdvertEntity> query() {
        Session session = null;

        List<TbAdvertEntity> list = null;
        try {
            //实例化Configuration，这行代码默认加载hibernate.cfg.xml文件
            Configuration conf = new Configuration().configure();

            //以Configuration创建SessionFactory
            SessionFactory sf = conf.buildSessionFactory();

            //实例化Session
            session = sf.openSession();

            String hql = "select  *  from tb_advert order by  advert_id desc ";

            Query query = session.createSQLQuery(hql);
            list = query.list();

        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return list;
    }




}
